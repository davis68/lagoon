/* jets/e/lms.c
**
*/
#include "all.h"
#include <math.h>  /* for sqrt */
#include <softfloat.h>
#include <stdio.h>
/*#include <atlas.h>*/

#define SINGNAN 0x7fc00000

  union trip {
    float32_t s;    //struct containing v, uint_32
    c3_w c;         //uint_32
    float b;       //float_32
  };

/* functions
*/
  static inline c3_t
  _nan_test(float32_t a)
  {
    return !f32_eq(a, a);
  }

  static inline float32_t
  _nan_unify(float32_t a)
  {
    if ( _nan_test(a) )
    {
      *(c3_w*)(&a) = SINGNAN;
    }
    return a;
  }

  static inline void
  _set_rounding(c3_w a)
  {
    switch ( a )
    {
    default:
      u3m_bail(c3__fail);
      break;
    case c3__n:
      softfloat_roundingMode = softfloat_round_near_even;
      break;
    case c3__z:
      softfloat_roundingMode = softfloat_round_minMag;
      break;
    case c3__u:
      softfloat_roundingMode = softfloat_round_max;
      break;
    case c3__d:
      softfloat_roundingMode = softfloat_round_min;
      break;
    }
  }

/* Cantor pairing function and inverse
*/
  u3_noun
  u3qelms_cantor(u3_atom m,  /* @ud */
                 u3_atom n)  /* @ud */
  {
    u3_atom p;
    p = (m+n)*(m+n+1)/2+n;

    return p;
  }

  u3_noun
  u3welms_cantor(u3_noun cor)
  {
    u3_noun a, b;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &a, u3x_sam_3, &b, 0) ||
         c3n == u3ud(a) ||
         c3n == u3ud(b) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_cantor(a,b);
    }
  }

  u3_noun
  u3qelms_decantor(u3_atom p)  /* @ud */
  {
    c3_w p_, w, t, m_, n_;

    p_ = u3r_word(0,p);
    w  = (c3_w)((sqrt((double)(8*p_+1))-1)/2);
    t  = (c3_w)((w*w+w)/2);
    n_ = p_-t;
    m_ = w-n_;
    u3_noun m, n;
    m = u3i_words(1, &m_);
    n = u3i_words(1, &n_);

    return u3nt(m, n, u3_nul);
  }

  u3_noun
  u3welms_decantor(u3_noun cor)
  {
    u3_noun a;

    if ( c3n == (a = u3r_at(u3x_sam, cor)) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_decantor(a);
    }
  }

/* zeros in @lms
*/
  u3_noun
  u3qelms_zeros(u3_atom m,  /* @ud */
                u3_atom n)  /* @ud */
  {
    c3_w m_ = u3r_word(0,m);
    c3_w n_ = u3r_word(0,n);
    c3_w* w_  = (c3_w*)u3a_malloc((m_*n_+1)*sizeof(float32_t));

    uint i;
    union trip c;
    c.c = 0x00000000;
    c3_w p_ = u3qelms_cantor(m_,n_);
    w_[m_*n_] = p_;
    for ( i = 0; i < n_*m_; i++ ) {
      w_[i] = (c3_w)c.s.v;
    }
    u3_noun w = u3i_words(m_*n_+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_zeros(u3_noun cor)
  {
    u3_noun a, b;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &a, u3x_sam_3, &b, 0) ||
         c3n == u3ud(a) ||
         c3n == u3ud(b) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_zeros(a, b);
    }
  }

/* ones in @lms
*/
  u3_noun
  u3qelms_ones(u3_atom m,  /* @ud */
               u3_atom n)  /* @ud */
  {
    c3_w m_ = u3r_word(0,m);
    c3_w n_ = u3r_word(0,n);
    c3_w* w_  = (c3_w*)u3a_malloc((m_*n_+1)*sizeof(float32_t));

    uint i;
    union trip c;
    c.c = 0x3F800000;
    c3_w p_ = u3qelms_cantor(m_,n_);
    w_[m_*n_] = p_;
    for ( i = 0; i < n_*m_; i++ ) {
      w_[i] = (c3_w)c.s.v;
    }
    u3_noun w = u3i_words(m_*n_+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_ones(u3_noun cor)
  {
    u3_noun a, b;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &a, u3x_sam_3, &b, 0) ||
         c3n == u3ud(a) ||
         c3n == u3ud(b) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_ones(a, b);
    }
  }

/* identity in @lms
*/
  u3_noun
  u3qelms_id(u3_atom m,  /* @ud */
             u3_atom n)  /* @ud */
  {
    c3_w m_ = u3r_word(0,m);
    c3_w n_ = u3r_word(0,n);
    c3_w* w_  = (c3_w*)u3a_malloc((m_*n_+1)*sizeof(float32_t));

    uint i;
    union trip c, d;
    c.c = 0x00000000;
    d.c = 0x3F800000;
    c3_w p_ = u3qelms_cantor(m_,n_);
    w_[m_*n_] = p_;
    for ( i = 0; i < n_*m_; i++ ) {
      if ( i % (n_+1) == 0 )
      {
        w_[i] = (c3_w)d.s.v;
      }
      else
      {
        w_[i] = (c3_w)c.s.v;
      }
    }
    u3_noun w = u3i_words(m_*n_+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_id(u3_noun cor)
  {
    u3_noun a, b;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &a, u3x_sam_3, &b, 0) ||
         c3n == u3ud(a) ||
         c3n == u3ud(b) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_id(a, b);
    }
  }

/* getter from @lms, 1-indexed
*/
  u3_noun
  u3qelms_get(u3_atom u,  /* @lms */
              u3_atom i,  /* @ud */
              u3_atom j)  /* @ud */
  {
    c3_w mnt_ = u3r_met(3,u)/4;  // mnt_ is the vector length
    c3_w i_ = u3r_word(0, i);
    c3_w j_ = u3r_word(0, j);

    union trip p_;
    p_.c = u3r_word(mnt_, u);
    u3_atom p__ = u3i_words(1,&(p_.c));

    u3_noun mn_ = u3qelms_decantor(p__);
    u3_noun m, n, o;
    u3r_trel(mn_, &m, &n, &o);
    c3_w m_, n_;
    m_ = u3r_word(0, m);  // XXX unnecessarily formal to unwrap like this
    n_ = u3r_word(0, n);

    if ((i_ < 1) || (i_ > m_) || (j_ < 1) || (j_ > n_))
    {
      return u3m_bail(c3__exit);
    }

    c3_w* w_  = (c3_w*)u3a_malloc(1*sizeof(float32_t));
    w_[0] = u3r_word((m_*n_)-((i_-1)*n_+j_), u);
    u3_atom w = u3i_words(1, w_);
    return w;
  }

  u3_noun
  u3welms_get(u3_noun cor)
  {
    u3_noun a, i, j;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &a, u3x_sam_6, &i, u3x_sam_7, &j, 0) ||
         c3n == u3ud(a) ||
         c3n == u3ud(i) ||
         c3n == u3ud(j) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_get(a, i, j);
    }
  }

/* setter for @lms, 1-indexed
*/
  u3_noun
  u3qelms_set(u3_atom u,  /* @lms */
              u3_atom i,  /* @ud */
              u3_atom j,  /* @ud */
              u3_atom a)  /* @rs */
  {
    c3_w mnt_ = u3r_met(3,u)/4;  // mnt_ is the vector length
    c3_w i_ = u3r_word(0, i);
    c3_w j_ = u3r_word(0, j);

    union trip p_;
    p_.c = u3r_word(mnt_, u);
    u3_atom p__ = u3i_words(1,&(p_.c));

    u3_noun mn_ = u3qelms_decantor(p__);
    u3_noun m, n, o;
    u3r_trel(mn_, &m, &n, &o);
    c3_w m_, n_;
    m_ = u3r_word(0, m);  // XXX unnecessarily formal to unwrap like this
    n_ = u3r_word(0, n);

    if ((i_ < 1) || (i_ > m_) || (j_ < 1) || (j_ > n_))
    {
      return u3m_bail(c3__exit);
    }

    c3_w* w_  = (c3_w*)u3a_malloc((m_*n_+1)*sizeof(float32_t));

    uint ii;
    w_[m_*n_] = p_.c;
    for ( ii = 0; ii < m_*n_; ii++ ) {
      if ( ii == ((m_*n_)-((i_-1)*n_+j_)) )
      {
        w_[ii] = u3r_word(0, a);
      }
      else
      {
        w_[ii] = u3r_word(ii, u);
      }
    }
    u3_noun w = u3i_words(m_*n_+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_set(u3_noun cor)
  {
    u3_noun u, i, j, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_14, &j, u3x_sam_15, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(j) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_set(u, i, j, a);
    }
  }

/* getter from @lms, 1-indexed
*/
  u3_noun
  u3qelms_getc(u3_atom u,  /* @lms */
               u3_atom j)  /* @ud */
  {
    c3_w mnt_u = u3r_met(3,u)/4;  // mnt_u is the matrix size (total)
    c3_w j_    = u3r_word(0, j);  // 1-indexed

    union trip p_u;
    p_u.c = u3r_word(mnt_u, u);
    u3_atom p__ = u3i_words(1,&(p_u.c));

    u3_noun mn_u = u3qelms_decantor(p__);
    u3_noun mu, nu, o;
    u3r_trel(mn_u, &mu, &nu, &o);
    c3_w m_u, n_u;
    m_u = u3r_word(0, mu);  // XXX unnecessarily formal to unwrap like this
    n_u = u3r_word(0, nu);
    c3_w n_v   = n_u;  // n_v is the vector length

    if ((j_ < 1) || (j_ > m_u))
    {
      return u3m_bail(c3__exit);
    }

    c3_w* w_  = (c3_w*)u3a_malloc((n_v+1)*sizeof(float32_t));

    c3_w ii, jj;
    w_[n_v] = n_v;
    ii = 1;
    for ( jj = 0; jj < m_u*n_u; jj++ ) {
      if ( ((m_u*n_u) - jj) % n_u == j_ % n_u )  // %n_u is b/c 1-indexed
      {
        w_[ii-1] = u3qelms_get(u, ii, j_);
        ii++;  if ( ii > n_v ) { break; }  // underflow check (0-indexed at 4294967295)
      }
    }
    u3_noun w = u3i_words(n_v+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_getc(u3_noun cor)
  {
    u3_noun a, i;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &a, u3x_sam_3, &i, 0) ||
         c3n == u3ud(a) ||
         c3n == u3ud(i) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_getc(a, i);
    }
  }

/* setter for @lms, 1-indexed
*/
  u3_noun
  u3qelms_setc(u3_atom u,  /* @lms */
               u3_atom j,  /* @ud */
               u3_atom v)  /* @lvs */
  {
    c3_w mnt_u = u3r_met(3,u)/4;  // mnt_u is the matrix size (total)
    c3_w n_v   = u3r_met(3,v)/4;  // n_v is the vector length
    c3_w j_    = u3r_word(0, j);
    // TODO: alternatively just use set to do this across the array

    union trip p_u;
    p_u.c = u3r_word(mnt_u, u);
    u3_atom p__ = u3i_words(1,&(p_u.c));

    u3_noun mn_u = u3qelms_decantor(p__);
    u3_noun mu, nu, o;
    u3r_trel(mn_u, &mu, &nu, &o);
    c3_w m_u, n_u;
    m_u = u3r_word(0, mu);  // XXX unnecessarily formal to unwrap like this
    n_u = u3r_word(0, nu);

    if ((j_ < 1) || (j_ > m_u) || !(n_v == n_u))
    {
      return u3m_bail(c3__exit);
    }

    c3_w* w_  = (c3_w*)u3a_malloc((m_u*n_u+1)*sizeof(float32_t));

    uint ii, jj;
    w_[m_u*n_u] = p_u.c;
    ii = n_v;
    for ( jj = 0; jj < m_u*n_u; jj++ ) {
      if ( ((m_u*n_u) - jj) % n_u == j_ % n_u )  // %n_u is b/c 1-indexed
      {
        w_[jj] = u3qelvs_get(v, ii);
        ii--;
      }
      else
      {
        w_[jj] = u3r_word(jj, u);
      }
    }
    u3_noun w = u3i_words(m_u*n_u+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_setc(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_setc(u, i, a);
    }
  }

/* getter from @lms, 1-indexed
*/
  u3_noun
  u3qelms_getr(u3_atom u,  /* @lms */
               u3_atom i)  /* @ud */
  {
    c3_w mnt_u = u3r_met(3,u)/4;  // mnt_u is the matrix size (total)
    c3_w i_    = u3r_word(0, i);  // 1-indexed

    union trip p_u;
    p_u.c = u3r_word(mnt_u, u);
    u3_atom p__ = u3i_words(1,&(p_u.c));

    u3_noun mn_u = u3qelms_decantor(p__);
    u3_noun mu, nu, o;
    u3r_trel(mn_u, &mu, &nu, &o);
    c3_w m_u, n_u;
    m_u = u3r_word(0, mu);  // XXX unnecessarily formal to unwrap like this
    n_u = u3r_word(0, nu);
    c3_w m_v   = m_u;  // m_v is the vector length

    if ((i_ < 1) || (i_ > m_u))
    {
      return u3m_bail(c3__exit);
    }

    c3_w* w_  = (c3_w*)u3a_malloc((m_v+1)*sizeof(float32_t));

    c3_w ii, jj;
    w_[m_v] = m_v;
    jj = 1;
    for ( ii = 0; ii < m_u*n_u; ii++ ) {
      if ( (((m_u*n_u) - i_*n_u) <= ii) && (ii < ((m_u*n_u) - (i_-1)*n_u)) )
      {
        w_[jj-1] = u3qelms_get(u, i_, jj);
        jj++;  if ( jj > m_v ) { break; }  // underflow check (0-indexed at 4294967295)
      }
    }
    u3_noun w = u3i_words(m_v+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_getr(u3_noun cor)
  {
    u3_noun a, i;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &a, u3x_sam_3, &i, 0) ||
         c3n == u3ud(a) ||
         c3n == u3ud(i) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_getr(a, i);
    }
  }

/* setter for @lms, 1-indexed
*/
  u3_noun
  u3qelms_setr(u3_atom u,  /* @lms */
               u3_atom i,  /* @ud */
               u3_atom v)  /* @lvs */
  {
    c3_w mnt_u = u3r_met(3,u)/4;  // mnt_u is the matrix size (total)
    c3_w n_v   = u3r_met(3,v)/4;  // n_v is the vector length
    c3_w i_    = u3r_word(0, i);

    union trip p_u;
    p_u.c = u3r_word(mnt_u, u);
    u3_atom p__ = u3i_words(1,&(p_u.c));

    u3_noun mn_u = u3qelms_decantor(p__);
    u3_noun mu, nu, o;
    u3r_trel(mn_u, &mu, &nu, &o);
    c3_w m_u, n_u;
    m_u = u3r_word(0, mu);  // XXX unnecessarily formal to unwrap like this
    n_u = u3r_word(0, nu);

    if ((i_ < 1) || (i_ > m_u) || !(n_v == n_u))
    {
      return u3m_bail(c3__exit);
    }

    c3_w* w_  = (c3_w*)u3a_malloc((m_u*n_u+1)*sizeof(float32_t));

    uint ii, jj;
    w_[m_u*n_u] = p_u.c;
    jj = n_v;
    for ( ii = 0; ii < m_u*n_u; ii++ ) {
      if ( (((m_u*n_u) - i_*n_u) <= ii) && (ii < ((m_u*n_u) - (i_-1)*n_u)) )
      {
        w_[ii] = (double)u3qelvs_get(v, jj);
        jj--;
      }
      else
      {
        w_[ii] = (double)u3r_word(ii, u);
      }
    }
    u3_noun w = u3i_words(m_u*n_u+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_setr(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_setr(u, i, a);
    }
  }

/* swap two columns
*/
  u3_noun
  u3qelms_swapc(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom j)  /* @ud */
  {
    //  XXX could be done faster as an in-place swap
    u3_noun ui = u3qelms_getc(u, i);
    u3_noun uj = u3qelms_getc(u, j);
    u3_noun ut = u3qelms_setc(u, i, uj);
    u = u3qelms_setc(ut, j, ui);

    return u;
  }

  u3_noun
  u3welms_swapc(u3_noun cor)
  {
    u3_noun u, i, j;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &j, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(j) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_swapc(u, i, j);
    }
  }

/* swap two rows
*/
  u3_noun
  u3qelms_swapr(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom j)  /* @ud */
  {
    //  XXX could be done faster as an in-place swap
    u3_noun ui = u3qelms_getr(u, i);
    u3_noun uj = u3qelms_getr(u, j);
    u3_noun ut = u3qelms_setr(u, i, uj);
    u = u3qelms_setr(ut, j, ui);

    return u;
  }

  u3_noun
  u3welms_swapr(u3_noun cor)
  {
    u3_noun u, i, j;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &j, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(j) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_swapr(u, i, j);
    }
  }

/* transpose
*/
  u3_noun
  u3qelms_trans(u3_atom u)  /* @lms */
  {
    c3_w mnt_u = u3r_met(3,u)/4;  // mnt_u is the matrix size (total)

    union trip p_u;
    p_u.c = u3r_word(mnt_u, u);
    u3_atom p__ = u3i_words(1,&(p_u.c));

    u3_noun mn_u = u3qelms_decantor(p__);
    u3_noun mu, nu, o;
    u3r_trel(mn_u, &mu, &nu, &o);
    c3_w m_u, n_u;
    m_u = u3r_word(0, mu);  // XXX unnecessarily formal to unwrap like this
    n_u = u3r_word(0, nu);

    uint i;
    u3_noun w = u3qelms_zeros(n_u, m_u);
    for ( i = 0; i < m_u; i++ )
    {
      w = u3qelms_setc(w, i, u3qelms_getr(u, i));
    }

    return w;
  }

  u3_noun
  u3welms_trans(u3_noun cor)
  {
    u3_noun u;

    if ( c3n == (u = u3r_at(u3x_sam, cor)) ||
         c3n == u3ud(u) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_trans(u);
    }
  }

/* add lms + scalar
*/
  u3_noun
  u3qelms_adds(u3_atom u,  /* @lms */
               u3_atom a,  /* @rs */
               u3_atom r)
  {
    c3_w mnt_ = u3r_met(3,u)/4;  // mnt_ is the vector length

    union trip p_;
    p_.c = u3r_word(mnt_, u);
    u3_atom p__ = u3i_words(1,&(p_.c));

    u3_noun mn_ = u3qelms_decantor(p__);
    u3_noun m, n, o;
    u3r_trel(mn_, &m, &n, &o);
    c3_w m_, n_;
    m_ = u3r_word(0, m);  // XXX unnecessarily formal to unwrap like this
    n_ = u3r_word(0, n);

    c3_w* w_  = (c3_w*)u3a_malloc((m_*n_+1)*sizeof(float32_t));

    uint i;
    union trip c, d;
    _set_rounding(r);
    d.c = u3r_word(0, a);
    w_[m_*n_] = p_.c;
    for ( i = 0; i < m_*n_; i++ ) {
      c.c = u3r_word(i, u);
      w_[i] = (c3_w)_nan_unify(f32_add(c.s,d.s)).v;
    }
    u3_noun w = u3i_words(m_*n_+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_adds(u3_noun cor)
  {
    u3_noun a, b;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &a, u3x_sam_3, &b, 0) ||
         c3n == u3ud(a) ||
         c3n == u3ud(b) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_adds(a, b, u3x_at(30, cor));
    }
  }

/* sub lms + scalar
*/
  u3_noun
  u3qelms_subs(u3_atom u,  /* @lms */
               u3_atom a,  /* @rs */
               u3_atom r)
  {
    c3_w mnt_ = u3r_met(3,u)/4;  // mnt_ is the vector length

    union trip p_;
    p_.c = u3r_word(mnt_, u);
    u3_atom p__ = u3i_words(1,&(p_.c));

    u3_noun mn_ = u3qelms_decantor(p__);
    u3_noun m, n, o;
    u3r_trel(mn_, &m, &n, &o);
    c3_w m_, n_;
    m_ = u3r_word(0, m);  // XXX unnecessarily formal to unwrap like this
    n_ = u3r_word(0, n);

    c3_w* w_  = (c3_w*)u3a_malloc((m_*n_+1)*sizeof(float32_t));

    uint i;
    union trip c, d;
    _set_rounding(r);
    d.c = u3r_word(0, a);
    w_[m_*n_] = p_.c;
    for ( i = 0; i < m_*n_; i++ ) {
      c.c = u3r_word(i, u);
      w_[i] = (c3_w)_nan_unify(f32_sub(c.s,d.s)).v;
    }
    u3_noun w = u3i_words(m_*n_+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_subs(u3_noun cor)
  {
    u3_noun a, b;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &a, u3x_sam_3, &b, 0) ||
         c3n == u3ud(a) ||
         c3n == u3ud(b) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_subs(a, b, u3x_at(30, cor));
    }
  }

/* mul lms + scalar
*/
  u3_noun
  u3qelms_muls(u3_atom u,  /* @lms */
               u3_atom a,  /* @rs */
               u3_atom r)
  {
    c3_w mnt_ = u3r_met(3,u)/4;  // mnt_ is the vector length

    union trip p_;
    p_.c = u3r_word(mnt_, u);
    u3_atom p__ = u3i_words(1,&(p_.c));

    u3_noun mn_ = u3qelms_decantor(p__);
    u3_noun m, n, o;
    u3r_trel(mn_, &m, &n, &o);
    c3_w m_, n_;
    m_ = u3r_word(0, m);  // XXX unnecessarily formal to unwrap like this
    n_ = u3r_word(0, n);

    c3_w* w_  = (c3_w*)u3a_malloc((m_*n_+1)*sizeof(float32_t));

    uint i;
    union trip c, d;
    _set_rounding(r);
    d.c = u3r_word(0, a);
    w_[m_*n_] = p_.c;
    for ( i = 0; i < m_*n_; i++ ) {
      c.c = u3r_word(i, u);
      w_[i] = (c3_w)_nan_unify(f32_mul(c.s,d.s)).v;
    }
    u3_noun w = u3i_words(m_*n_+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_muls(u3_noun cor)
  {
    u3_noun a, b;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &a, u3x_sam_3, &b, 0) ||
         c3n == u3ud(a) ||
         c3n == u3ud(b) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_muls(a, b, u3x_at(30, cor));
    }
  }

/* div lms + scalar
*/
  u3_noun
  u3qelms_divs(u3_atom u,  /* @lms */
               u3_atom a,  /* @rs */
               u3_atom r)
  {
    c3_w mnt_ = u3r_met(3,u)/4;  // mnt_ is the vector length

    union trip p_;
    p_.c = u3r_word(mnt_, u);
    u3_atom p__ = u3i_words(1,&(p_.c));

    u3_noun mn_ = u3qelms_decantor(p__);
    u3_noun m, n, o;
    u3r_trel(mn_, &m, &n, &o);
    c3_w m_, n_;
    m_ = u3r_word(0, m);  // XXX unnecessarily formal to unwrap like this
    n_ = u3r_word(0, n);

    c3_w* w_  = (c3_w*)u3a_malloc((m_*n_+1)*sizeof(float32_t));

    uint i;
    union trip c, d;
    _set_rounding(r);
    d.c = u3r_word(0, a);
    w_[m_*n_] = p_.c;
    for ( i = 0; i < m_*n_; i++ ) {
      c.c = u3r_word(i, u);
      w_[i] = (c3_w)_nan_unify(f32_div(c.s,d.s)).v;
    }
    u3_noun w = u3i_words(m_*n_+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_divs(u3_noun cor)
  {
    u3_noun a, b;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &a, u3x_sam_3, &b, 0) ||
         c3n == u3ud(a) ||
         c3n == u3ud(b) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_divs(a, b, u3x_at(30, cor));
    }
  }

/* add lms + rs at col
*/
  u3_noun
  u3qelms_addsc(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom a,  /* @rs */
                u3_atom r)
  {
    return u3qelms_setc(u, i, u3qelvs_adds(u3qelms_getc(u, i), a, r));
  }

  u3_noun
  u3welms_addsc(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_addsc(u, i, a, u3x_at(30, cor));
    }
  }

/* sub lms + rs at col
*/
  u3_noun
  u3qelms_subsc(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom a,  /* @rs */
                u3_atom r)
  {
    return u3qelms_setc(u, i, u3qelvs_subs(u3qelms_getc(u, i), a, r));
  }

  u3_noun
  u3welms_subsc(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_subsc(u, i, a, u3x_at(30, cor));
    }
  }

/* mul lms + rs at col
*/
  u3_noun
  u3qelms_mulsc(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom a,  /* @rs */
                u3_atom r)
  {
    return u3qelms_setc(u, i, u3qelvs_muls(u3qelms_getc(u, i), a, r));
  }

  u3_noun
  u3welms_mulsc(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_mulsc(u, i, a, u3x_at(30, cor));
    }
  }

/* div lms + rs at col
*/
  u3_noun
  u3qelms_divsc(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom a,  /* @rs */
                u3_atom r)
  {
    return u3qelms_setc(u, i, u3qelvs_divs(u3qelms_getc(u, i), a, r));
  }

  u3_noun
  u3welms_divsc(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_divsc(u, i, a, u3x_at(30, cor));
    }
  }

/* add lms + rs at row
*/
  u3_noun
  u3qelms_addsr(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom a,  /* @rs */
                u3_atom r)
  {
    return u3qelms_setr(u, i, u3qelvs_adds(u3qelms_getr(u, i), a, r));
  }

  u3_noun
  u3welms_addsr(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_addsr(u, i, a, u3x_at(30, cor));
    }
  }

/* sub lms + rs at row
*/
  u3_noun
  u3qelms_subsr(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom a,  /* @rs */
                u3_atom r)
  {
    return u3qelms_setr(u, i, u3qelvs_subs(u3qelms_getr(u, i), a, r));
  }

  u3_noun
  u3welms_subsr(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_subsr(u, i, a, u3x_at(30, cor));
    }
  }

/* mul lms + rs at row
*/
  u3_noun
  u3qelms_mulsr(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom a,  /* @rs */
                u3_atom r)
  {
    return u3qelms_setr(u, i, u3qelvs_muls(u3qelms_getr(u, i), a, r));
  }

  u3_noun
  u3welms_mulsr(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_mulsr(u, i, a, u3x_at(30, cor));
    }
  }

/* div lms + rs at row
*/
  u3_noun
  u3qelms_divsr(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom a,  /* @rs */
                u3_atom r)
  {
    return u3qelms_setr(u, i, u3qelvs_divs(u3qelms_getr(u, i), a, r));
  }

  u3_noun
  u3welms_divsr(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_divsr(u, i, a, u3x_at(30, cor));
    }
  }

/* add lms + lvs at col
*/
  u3_noun
  u3qelms_addvc(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom a,  /* @lvs */
                u3_atom r)
  {
    return u3qelms_setc(u, i, u3qelvs_addv(u3qelms_getc(u, i), a, r));
  }

  u3_noun
  u3welms_addvc(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_addvc(u, i, a, u3x_at(30, cor));
    }
  }

/* sub lms + lvs at col
*/
  u3_noun
  u3qelms_subvc(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom a,  /* @lvs */
                u3_atom r)
  {
    return u3qelms_setc(u, i, u3qelvs_subv(u3qelms_getc(u, i), a, r));
  }

  u3_noun
  u3welms_subvc(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_subvc(u, i, a, u3x_at(30, cor));
    }
  }

/* mul lms + lvs at col
*/
  u3_noun
  u3qelms_mulvc(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom a,  /* @lvs */
                u3_atom r)
  {
    return u3qelms_setc(u, i, u3qelvs_mulv(u3qelms_getc(u, i), a, r));
  }

  u3_noun
  u3welms_mulvc(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_mulvc(u, i, a, u3x_at(30, cor));
    }
  }

/* div lms + lvs at col
*/
  u3_noun
  u3qelms_divvc(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom a,  /* @lvs */
                u3_atom r)
  {
    return u3qelms_setc(u, i, u3qelvs_divv(u3qelms_getc(u, i), a, r));
  }

  u3_noun
  u3welms_divvc(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_divvc(u, i, a, u3x_at(30, cor));
    }
  }

/* add lms + lvs at row
*/
  u3_noun
  u3qelms_addvr(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom a,  /* @lvs */
                u3_atom r)
  {
    return u3qelms_setr(u, i, u3qelvs_addv(u3qelms_getr(u, i), a, r));
  }

  u3_noun
  u3welms_addvr(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_addvr(u, i, a, u3x_at(30, cor));
    }
  }

/* sub lms + lvs at row
*/
  u3_noun
  u3qelms_subvr(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom a,  /* @lvs */
                u3_atom r)
  {
    return u3qelms_setr(u, i, u3qelvs_subv(u3qelms_getr(u, i), a, r));
  }

  u3_noun
  u3welms_subvr(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_subvr(u, i, a, u3x_at(30, cor));
    }
  }

/* mul lms + lvs at col
*/
  u3_noun
  u3qelms_mulvr(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom a,  /* @lvs */
                u3_atom r)
  {
    return u3qelms_setr(u, i, u3qelvs_mulv(u3qelms_getr(u, i), a, r));
  }

  u3_noun
  u3welms_mulvr(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_mulvr(u, i, a, u3x_at(30, cor));
    }
  }

/* div lms + lvs at row
*/
  u3_noun
  u3qelms_divvr(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom a,  /* @lvs */
                u3_atom r)
  {
    return u3qelms_setr(u, i, u3qelvs_divv(u3qelms_getr(u, i), a, r));
  }

  u3_noun
  u3welms_divvr(u3_noun cor)
  {
    u3_noun u, i, a;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &a, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(a) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_divvr(u, i, a, u3x_at(30, cor));
    }
  }

/* add lms + lms
*/
  u3_noun
  u3qelms_addm(u3_atom u,  /* @lms */
               u3_atom v,  /* @lms */
               u3_atom r)
  {
    c3_w mnt_u = u3r_met(3,u)/4;  // mnt_u is the matrix size (total)
    c3_w mnt_v = u3r_met(3,v)/4;  // mnt_v is the matrix size (total)

    union trip p_u, p_v;
    p_u.c = u3r_word(mnt_u, u);
    p_v.c = u3r_word(mnt_v, v);
    u3_atom p__u = u3i_words(1,&(p_u.c));
    u3_atom p__v = u3i_words(1,&(p_v.c));

    if ( (mnt_u != mnt_v) || (p__u != p__v) )
    {
      return u3m_bail(c3__exit);
    }

    u3_noun mn_u = u3qelms_decantor(p__u);
    u3_noun mu, nu, o;
    u3r_trel(mn_u, &mu, &nu, &o);
    c3_w m_u, n_u;
    m_u = u3r_word(0, mu);  // XXX unnecessarily formal to unwrap like this
    n_u = u3r_word(0, nu);

    c3_w* w_  = (c3_w*)u3a_malloc((m_u*n_u+1)*sizeof(float32_t));

    union trip c, d;
    c3_w ii, jj, index;
    w_[m_u*n_u] = p_u.c;
    for ( ii = 0; ii < n_u; ii++ )
    {
      for ( jj = 0; jj < m_u; jj++ )
      {
        index = ii*n_u+jj;
        d.c = u3r_word(index, u);
        c.c = u3r_word(index, v);
        w_[index] = (c3_w)_nan_unify(f32_add(c.s,d.s)).v;
      }
    }
    u3_noun w = u3i_words(m_u*n_u+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_addm(u3_noun cor)
  {
    u3_noun u, v;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_3, &v, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(v) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_addm(u, v, u3x_at(30, cor));
    }
  }

/* sub lms + lms
*/
  u3_noun
  u3qelms_subm(u3_atom u,  /* @lms */
               u3_atom v,  /* @lms */
               u3_atom r)
  {
    c3_w mnt_u = u3r_met(3,u)/4;  // mnt_u is the matrix size (total)
    c3_w mnt_v = u3r_met(3,v)/4;  // mnt_v is the matrix size (total)

    union trip p_u, p_v;
    p_u.c = u3r_word(mnt_u, u);
    p_v.c = u3r_word(mnt_v, v);
    u3_atom p__u = u3i_words(1,&(p_u.c));
    u3_atom p__v = u3i_words(1,&(p_v.c));

    if ( (mnt_u != mnt_v) || (p__u != p__v) )
    {
      return u3m_bail(c3__exit);
    }

    u3_noun mn_u = u3qelms_decantor(p__u);
    u3_noun mu, nu, o;
    u3r_trel(mn_u, &mu, &nu, &o);
    c3_w m_u, n_u;
    m_u = u3r_word(0, mu);  // XXX unnecessarily formal to unwrap like this
    n_u = u3r_word(0, nu);

    c3_w* w_  = (c3_w*)u3a_malloc((m_u*n_u+1)*sizeof(float32_t));

    union trip c, d;
    c3_w ii, jj, index;
    w_[m_u*n_u] = p_u.c;
    for ( ii = 0; ii < n_u; ii++ )
    {
      for ( jj = 0; jj < m_u; jj++ )
      {
        index = ii*n_u+jj;
        d.c = u3r_word(index, u);
        c.c = u3r_word(index, v);
        w_[index] = (c3_w)_nan_unify(f32_sub(c.s,d.s)).v;
      }
    }
    u3_noun w = u3i_words(m_u*n_u+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_subm(u3_noun cor)
  {
    u3_noun u, v;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_3, &v, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(v) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_subm(u, v, u3x_at(30, cor));
    }
  }

/* mul lms + lms
*/
  u3_noun
  u3qelms_mulm(u3_atom u,  /* @lms */
               u3_atom v,  /* @lms */
               u3_atom r)
  {
    c3_w mnt_u = u3r_met(3,u)/4;  // mnt_u is the matrix size (total)
    c3_w mnt_v = u3r_met(3,v)/4;  // mnt_v is the matrix size (total)

    union trip p_u, p_v;
    p_u.c = u3r_word(mnt_u, u);
    p_v.c = u3r_word(mnt_v, v);
    u3_atom p__u = u3i_words(1,&(p_u.c));
    u3_atom p__v = u3i_words(1,&(p_v.c));

    if ( (mnt_u != mnt_v) || (p__u != p__v) )
    {
      return u3m_bail(c3__exit);
    }

    u3_noun mn_u = u3qelms_decantor(p__u);
    u3_noun mu, nu, o;
    u3r_trel(mn_u, &mu, &nu, &o);
    c3_w m_u, n_u;
    m_u = u3r_word(0, mu);  // XXX unnecessarily formal to unwrap like this
    n_u = u3r_word(0, nu);

    c3_w* w_  = (c3_w*)u3a_malloc((m_u*n_u+1)*sizeof(float32_t));

    union trip c, d;
    c3_w ii, jj, index;
    w_[m_u*n_u] = p_u.c;
    for ( ii = 0; ii < n_u; ii++ )
    {
      for ( jj = 0; jj < m_u; jj++ )
      {
        index = ii*n_u+jj;
        d.c = u3r_word(index, u);
        c.c = u3r_word(index, v);
        w_[index] = (c3_w)_nan_unify(f32_mul(c.s,d.s)).v;
      }
    }
    u3_noun w = u3i_words(m_u*n_u+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_mulm(u3_noun cor)
  {
    u3_noun u, v;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_3, &v, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(v) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_mulm(u, v, u3x_at(30, cor));
    }
  }

/* div lms + lms
*/
  u3_noun
  u3qelms_divm(u3_atom u,  /* @lms */
               u3_atom v,  /* @lms */
               u3_atom r)
  {
    c3_w mnt_u = u3r_met(3,u)/4;  // mnt_u is the matrix size (total)
    c3_w mnt_v = u3r_met(3,v)/4;  // mnt_v is the matrix size (total)

    union trip p_u, p_v;
    p_u.c = u3r_word(mnt_u, u);
    p_v.c = u3r_word(mnt_v, v);
    u3_atom p__u = u3i_words(1,&(p_u.c));
    u3_atom p__v = u3i_words(1,&(p_v.c));

    if ( (mnt_u != mnt_v) || (p__u != p__v) )
    {
      return u3m_bail(c3__exit);
    }

    u3_noun mn_u = u3qelms_decantor(p__u);
    u3_noun mu, nu, o;
    u3r_trel(mn_u, &mu, &nu, &o);
    c3_w m_u, n_u;
    m_u = u3r_word(0, mu);  // XXX unnecessarily formal to unwrap like this
    n_u = u3r_word(0, nu);

    c3_w* w_  = (c3_w*)u3a_malloc((m_u*n_u+1)*sizeof(float32_t));

    union trip c, d;
    c3_w ii, jj, index;
    w_[m_u*n_u] = p_u.c;
    for ( ii = 0; ii < n_u; ii++ )
    {
      for ( jj = 0; jj < m_u; jj++ )
      {
        index = ii*n_u+jj;
        d.c = u3r_word(index, u);
        c.c = u3r_word(index, v);
        w_[index] = (c3_w)_nan_unify(f32_div(c.s,d.s)).v;
      }
    }
    u3_noun w = u3i_words(m_u*n_u+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_divm(u3_noun cor)
  {
    u3_noun u, v;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_3, &v, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(v) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_divm(u, v, u3x_at(30, cor));
    }
  }

/* matrix multiply lms + lms
   XXX this one would benefit from ATLAS/BLAS
*/
  u3_noun
  u3qelms_mmul(u3_atom u,  /* @lms */
               u3_atom v,  /* @lms */
               u3_atom r)
  {
    c3_w mnt_u = u3r_met(3,u)/4;  // mnt_u is the matrix size (total)
    c3_w mnt_v = u3r_met(3,v)/4;  // mnt_v is the matrix size (total)

    union trip p_u, p_v;
    p_u.c = u3r_word(mnt_u, u);
    p_v.c = u3r_word(mnt_v, v);
    u3_atom p__u = u3i_words(1,&(p_u.c));
    u3_atom p__v = u3i_words(1,&(p_v.c));

    if ( (mnt_u != mnt_v) || (p__u != p__v) )
    {
      return u3m_bail(c3__exit);
    }

    u3_noun mn_u = u3qelms_decantor(p__u);
    u3_noun mu, nu, o;
    u3r_trel(mn_u, &mu, &nu, &o);
    c3_w m_u, n_u;
    m_u = u3r_word(0, mu);  // XXX unnecessarily formal to unwrap like this
    n_u = u3r_word(0, nu);

    u3_noun mn_v = u3qelms_decantor(p__v);
    u3_noun mv, nv;
    u3r_trel(mn_v, &mv, &nv, &o);
    c3_w m_v, n_v;
    m_v = u3r_word(0, mv);  // XXX unnecessarily formal to unwrap like this
    n_v = u3r_word(0, nv);

    if (n_u != m_v )
    {
      return u3m_bail(c3__exit);
    }

    c3_w* w_  = (c3_w*)u3a_malloc((m_u*n_u+1)*sizeof(float32_t));

    union trip c, d, sum;
    c3_w ii, jj, kk;
    w_[m_u*n_v] = p_u.c;
    for ( ii = 0; ii < m_u; ii++ )
    {
      for ( jj = 0; jj < n_v; jj++ )
      {
        sum.c = 0x00000000;
        for ( kk = 0; kk < m_v; kk++ )
        {
          d.c = u3r_word(ii*n_u+kk, u);
          c.c = u3r_word(kk*n_v+jj, v);
          sum.c = (c3_w)_nan_unify(f32_add(sum.s,f32_mul(c.s,d.s))).v;
        }
        w_[ii*n_u+jj] = sum.c;
      }
    }
    u3_noun w = u3i_words(m_u*n_v+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_mmul(u3_noun cor)
  {
    u3_noun u, v;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_3, &v, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(v) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_mmul(u, v, u3x_at(30, cor));
    }
  }

/* matrix exponentiation (A**N, not e(A))
*/
  u3_noun
  u3qelms_mpow(u3_atom u,  /* @lms */
               u3_atom i,  /* @ud */
               u3_atom r)
  {
    c3_w i_, ii;
    i_ = u3r_word(0, i);

    u3_atom w = u;
    for ( ii = 0; ii < i_; ii++ )
    {
      w = u3qelms_mmul(u, w, r);
    }

    return w;
  }

  u3_noun
  u3welms_mpow(u3_noun cor)
  {
    u3_noun u, i;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_3, &i, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_mpow(u, i, u3x_at(30, cor));
    }
  }

/* matrix minor
*/
  u3_noun
  u3qelms_minor(u3_atom u,  /* @lms */
                u3_atom i,  /* @ud */
                u3_atom j)  /* @ud */
  {
    c3_w mnt_ = u3r_met(3,u)/4;  // mnt_ is the vector length
    c3_w i_ = u3r_word(0, i);
    c3_w j_ = u3r_word(0, j);

    union trip p_;
    p_.c = u3r_word(mnt_, u);
    u3_atom p__ = u3i_words(1,&(p_.c));

    u3_noun mn_ = u3qelms_decantor(p__);
    u3_noun m, n, o;
    u3r_trel(mn_, &m, &n, &o);
    c3_w m_u, n_u;
    m_u = u3r_word(0, m);  // XXX unnecessarily formal to unwrap like this
    n_u = u3r_word(0, n);

    if ((i_ < 1) || (i_ > m_u) || (j_ < 1) || (j_ > n_u))
    {
      return u3m_bail(c3__exit);
    }

    c3_w m_w, n_w;
    m_w = m_u - 1;
    n_w = n_u - 1;
    c3_w* w_  = (c3_w*)u3a_malloc((m_w*n_w+1)*sizeof(float32_t));
    c3_w p_w = u3qelms_cantor(m_w, n_w);
    w_[m_w*n_w] = p_w;
    c3_w ii, jj, mi, mj;
    i_ = m_u - i_ + 1;
    j_ = n_u - j_ + 1;
    for ( ii = 0; ii < m_u; ii++ )
    {
      mi = ii > (i_-1) ? ii - 1 : ii;
      for ( jj = 0; jj < n_u; jj++ )
      {
        mj = jj > (j_-1) ? jj - 1 : jj;
        if ( (ii == (i_-1)) || (jj == (j_-1)) ) continue;
        w_[mi*n_w+mj] = u3r_word(ii*n_u+jj, u);
      }
    }
    u3_noun w = u3i_words(m_w*n_w+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_minor(u3_noun cor)
  {
    u3_noun u, i, j;

    if ( c3n == u3r_mean(cor, u3x_sam_2, &u, u3x_sam_6, &i, u3x_sam_7, &j, 0) ||
         c3n == u3ud(u) ||
         c3n == u3ud(i) ||
         c3n == u3ud(j) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_minor(u, i, j);
    }
  }

/* augment matrix
*/
  u3_noun
  u3qelms_augment(u3_atom u)  /* @lms */
  {
    c3_w mnt_ = u3r_met(3,u)/4;  // mnt_ is the vector length

    union trip p_;
    p_.c = u3r_word(mnt_, u);
    u3_atom p__ = u3i_words(1,&(p_.c));

    u3_noun mn_ = u3qelms_decantor(p__);
    u3_noun m, n, o;
    u3r_trel(mn_, &m, &n, &o);
    c3_w m_u, n_u, m_w, n_w;
    m_u = u3r_word(0, m);  // XXX unnecessarily formal to unwrap like this
    n_u = u3r_word(0, n);
    m_w = m_u;
    n_w = 2*n_u;

    u3_atom v = u3qelms_id(m_u, n_u);

    c3_w* w_  = (c3_w*)u3a_malloc((m_w*n_w+1)*sizeof(float32_t));
    c3_w p_w = u3qelms_cantor(m_w, n_w);
    w_[m_w*n_w] = p_w;
    c3_w ii, jj;
    for ( ii = 0; ii < m_u; ii++ )
    {
      for ( jj = 0; jj < n_u; jj++ )
      {
        w_[ii*n_w+n_u+jj] = u3r_word(ii*n_u+jj, u);
      }
      for ( jj = 0; jj < n_u; jj++ )
      {
        w_[ii*n_w+jj] = u3r_word(ii*n_u+jj, v);
      }
    }
    u3_noun w = u3i_words(m_w*n_w+1, w_);
    u3a_free(w_);

    return w;
  }

  u3_noun
  u3welms_augment(u3_noun cor)
  {
    u3_noun u;

    if ( c3n == (u = u3r_at(u3x_sam, cor)) ||
         c3n == u3ud(u) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_augment(u);
    }
  }

/* invert matrix
   XXX this should become a wrapper not an implementation
*/
  u3_noun
  u3qelms_invert(u3_atom u)  /* @lms */
  {
    c3_w mnt_ = u3r_met(3,u)/4;  // mnt_ is the vector length

    union trip p_;
    p_.c = u3r_word(mnt_, u);
    u3_atom p__ = u3i_words(1,&(p_.c));

    u3_noun mn_ = u3qelms_decantor(p__);
    u3_noun m, n, o;
    u3r_trel(mn_, &m, &n, &o);
    c3_w m_u, n_u;
    m_u = u3r_word(0, m);  // XXX unnecessarily formal to unwrap like this
    n_u = u3r_word(0, n);

    if ( m_u != n_u )
    {
      return u3m_bail(c3__exit);
    }

    u3_atom v = u3qelms_augment(u);
    c3_w m_v, n_v;
    m_v = m_u;
    n_v = 2*n_u;

    //  Flip the order around so it feels natural.
    //  This isn't formally necessary, but it's a lot easier to think about.
    c3_w* w__  = (c3_w*)u3a_malloc((m_u*n_v+1)*sizeof(float32_t));
    u3r_words(0, m_u*n_v+1, w__, v);
    c3_w* w_  = (c3_w*)u3a_malloc((m_u*n_v+1)*sizeof(float32_t));
    w_[m_u*n_u] = w__[m_u*n_u];
    c3_w ii, jj, kk, max_row;
    for ( ii = 0; ii < m_u*n_v; ii++ )
    {
      w_[m_u*n_v-1-ii] = w__[ii];
    }
    u3a_free(w__);

    union trip el, max_el, t, c, d, e;
    /*for ( ii = 0; ii < m_u; ii++ )
    {
      for ( jj = 0; jj < n_v; jj++ )
      {
        max_el.c = abs(w_[ii*n_v+jj]);
        fprintf(stderr,"%u/%u %f - ",ii,jj,max_el.b);
      }
      fprintf(stderr,"\n");
    }*/
    for ( ii = 0; ii < m_u; ii++ )
    {
      //  Locate maximum value in column for pivot.
      max_el.c = abs(w_[ii*n_v+ii]);
      max_row = ii;
      fprintf(stderr, "%u %f %u\n", ii, max_el.b, max_row);
      for ( kk = ii+1; kk < m_u; kk++ )
      {
        //fprintf(stderr, "> %u %u %u/%u\n", ii, kk,n kk*n_v+ii, m_u*n_v);
        el.c = abs(w_[kk*n_v+ii]);
        //fprintf(stderr, "> %u %u %u/%u: %f\n", ii, kk, kk*n_v+ii, m_u*n_v, el.b);
        if ( el.b > max_el.b ) {
          max_el.c = abs(w_[kk*n_v+ii]);
          max_row = kk;
          fprintf(stderr, "> %u %u %u\n", ii, kk, kk*n_v+ii);
        }
      }
      //  Swap max row with current row.
      for ( kk = ii; kk < n_v; kk++ )
      {
        //fprintf(stderr, "» %u %u %u\n", ii, kk, max_row*n_v+kk);
        t.c = w_[max_row*n_v+kk];
        //fprintf(stderr, "» %u %u %u\n", ii, kk, max_row*n_v+kk);
        w_[max_row*n_v+kk] = w_[ii*n_v+kk];
        //fprintf(stderr, "» %u %u %u\n", ii, kk, max_row*n_v+kk);
        w_[ii*n_v+kk] = t.c;
        //fprintf(stderr, "» %u %u %u\n", ii, kk, max_row*n_v+kk);
      }
      //  Set all rows below this to zero in current column.
      for ( kk = ii+1; kk < n_v; kk++ )
      {
        //fprintf(stderr, "here!!!!!!!!!!!!!!!!!!!!\n");
        c.c = -w_[ii*n_v+kk] / w_[ii*n_v+ii];
        for ( jj = ii; jj < m_u+1; jj++ )
        {
          //fprintf(stderr, "    %u!!!!!!!!!!!!!!!!!!!\n", jj);
          d.c = w_[ii*n_v+jj];
          if ( ii == jj )
          {
            w_[kk*n_v+jj] = 0;
          }
          else
          {
            e.c = w_[kk*n_v+jj];
            w_[kk*n_v+jj] = (c3_w)_nan_unify(f32_add(e.s, f32_mul(c.s,d.s))).v;
          }
        }
      }
    }
    // For each row, divide by leading value
    fprintf(stderr, "divide by leading value-------------------------------\n");
    for ( ii = 0; ii < m_u; ii++ )
    {
      c.c = w_[ii*n_v+ii];
      for ( jj = ii; jj < n_v; jj++ )
      {
        d.c = w_[ii*n_v+jj];
        //e.s = f32_div(d.s, c.s);
        w_[ii*n_v+jj] = (c3_w)_nan_unify(f32_div(d.s, c.s)).v;
        //fprintf(stderr,"%u/%u %f ÷ %f = %f\n",ii,jj,c.b,d.b,e.b);
      }
    }
    for ( ii = 0; ii < m_u; ii++ )
    {
      for ( jj = 0; jj < n_v; jj++ )
      {
        c.c = abs(w_[ii*n_v+jj]);
        fprintf(stderr,"%f ",c.b);
      }
      fprintf(stderr,"\n");
    }
    // then replace up
    //  rowj = rowj - rowi*elemij
    fprintf(stderr, "replace up--------------------------------------------\n");
    for ( ii = m_u-1; ii >= 0 && ii < 4294967295; ii-- )  // watch underflow
    {
      c.c = w_[ii*n_v+ii];
      for ( jj = ii-1; jj >= 0 && jj < 4294967295; jj-- )  // watch underflow
      {
        fprintf(stderr, "***\n");

        fprintf(stderr,"%u %u @ %f\n",ii,jj,c.b);
        for ( kk = ii; kk < n_v; kk++ )
        {
          union trip f, g;
          d.c = w_[ii*n_v+kk];
          e.c = w_[jj*n_v+kk];
          //e.s = f32_sub(d.s, c.s);
          f.s = f32_mul(d.s, c.s);
          g.s = f32_sub(d.s, f32_mul(d.s,c.s));
          fprintf(stderr,"> %u,%u,%u @ %f %f %f %f %f\n",ii,jj,kk,c.b,d.b,e.b,f.b,g.b);
          w_[jj*n_v+kk] = (c3_w)_nan_unify(f32_sub(e.s, f32_mul(d.s,e.s))).v;
        }
      }
    }
    fprintf(stderr, "***\n");
    for ( ii = 0; ii < m_u; ii++ )
    {
      for ( jj = 0; jj < n_v; jj++ )
      {
        c.c = abs(w_[ii*n_v+jj]);
        fprintf(stderr,"%f ",c.b);
      }
      fprintf(stderr,"\n");
    }
    u3_noun w = u3i_words(m_u*n_v+1, w_);
    fprintf(stderr, "-------------------------------\n");
    u3a_free(w_);
    fprintf(stderr, "-------------------------------\n");

    return w;
  }

  u3_noun
  u3welms_invert(u3_noun cor)
  {
    u3_noun u;

    if ( c3n == (u = u3r_at(u3x_sam, cor)) ||
         c3n == u3ud(u) )
    {
      return u3m_bail(c3__exit);
    }
    else {
      return u3qelms_invert(u);
    }
  }
