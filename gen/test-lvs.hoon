::  %say generator for studying and verifying the Lagoon linear algebra library.
::
/+  *lv
:-  %say
|=  [[now=@da eny=@uvJ bec=beak] [~] [n=@ud ~]]
:-  %noun
::  As you test each of these, sigpam them to add another.
=/  v0  (zeros:lvs 5)
=/  v1  (ones:lvs 5)
=/  vr  (make:lvs `(list @rs)`~[.1 .2 .3 .4 .5])
::~&  v0
::~&  v1
::~&  vr
::~&  (addv:lvd v1 vr)
::~&  (subv:lvd v1 vr)
::~&  (mulv:lvd v1 vr)
::~&  (divv:lvd v1 vr)
::~&  (adds:lvd vr .~10)
::~&  (subs:lvd vr .~10)
::~&  (muls:lvd vr .~10)
::~&  (divs:lvd vr .~10)
::~&  (inner:lvd vr vr)
~&  vr
~&  (set:lvs vr 3 .8)
~&  (unmake:lvs (set:lvs vr 3 .8))
(make:lvs `(list @rs)`~[.1 .2 .3 .4 .5])

:: Is there a way to pin zero-bits onto a value?  Holding zeros at the top of an atom is not trivial unless we pin the length on first...

:: To convert, change @rd to @rs etc. and 6 to 5 throughout.  Rename @lvd to @lvs.
