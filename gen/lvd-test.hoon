::  %say generator for studying and verifying the Lagoon linear algebra library.
::
/+  lagoon-lvd
:-  %say
|=  [[now=@da eny=@uvJ bec=beak] [~] [n=@ud ~]]
:-  %noun
=/  vec-a  `(list @rd)`~[.~1 .~2 .~3 .~4 .~5]
::  As you test each of these, sigpam them to add another.
~&  (length:lagoon-lvd vec-a)
~&  (get:lagoon-lvd vec-a 2)
~&  (set:lagoon-lvd vec-a [4 .~9])
~&  (swap:lagoon-lvd vec-a 4 3)
~&  (adds:lagoon-lvd vec-a .~10)
~&  (subs:lagoon-lvd vec-a .~10)
~&  (muls:lagoon-lvd vec-a .~10)
~&  (divs:lagoon-lvd vec-a .~10)
=/  vec-b  `(list @rd)`~[.~6 .~7 .~8 .~9 .~10]
~&  (addv:lagoon-lvd vec-a vec-b)
~&  (subv:lagoon-lvd vec-a vec-b)
~&  (mulv:lagoon-lvd vec-a vec-b)
~&  (divv:lagoon-lvd vec-a vec-b)
~&  (inner:lagoon-lvd vec-a vec-b)
~&  (sum:lagoon-lvd vec-a)
vec-a
