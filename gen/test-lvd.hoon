::  %say generator for studying and verifying the Lagoon linear algebra library.
::
/+  *lv
:-  %say
|=  [[now=@da eny=@uvJ bec=beak] [~] [n=@ud ~]]
:-  %noun
::  As you test each of these, sigpam them to add another.
=/  v0  (zeros:lvd 6)
=/  v1  (ones:lvd 6)
~&  "vr:"
=/  vr  (make:lvd `(list @rd)`~[.~10 .~11 .~12 .~13 .~14 .~15])
~&  vr
~&  (addv:lvd v1 vr)
~&  "v200:"
=/  v10  (ones:lvd 10)
=/  v20  (addv:lvd v10 v10)
~&  v20
::~&  (subv:lvd v1 vr)
::~&  (mulv:lvd v1 vr)
::~&  (divv:lvd v1 vr)
~&  (adds:lvd vr .~20)
::~&  (unmake:lvd `@lvd`(adds:lvd vr .~20))
~&  (length:lvd v20)
~&  (unmake:lvd v20)
~&  (unmake:lvd `@lvd`(addv:lvd v10 v10))
::~&  (subs:lvd vr .~10)
::~&  (muls:lvd vr .~10)
::~&  (divs:lvd vr .~10)
::~&  (inner:lvd vr vr)
::~&  vr
~&  (set:lvd vr 3 .~8)
~&  (unmake:lvd (set:lvd vr 3 .~8))
::~&  (get:lvd vr 0)
~&  (get:lvd vr 1)
~&  (get:lvd vr 2)
~&  (get:lvd vr 3)
~&  (get:lvd vr 4)
~&  (get:lvd vr 5)
~&  (get:lvd vr 6)
::~&  (unmake:lvd (set:lvd vr 3 .~8))
::(make:lvd `(list @rd)`~[.~1 .~2 .~3 .~4 .~5])
1
