::  %say generator for studying and verifying the Lagoon linear algebra library.
::
/+  lvd,lmd
!:
:-  %say
|=  [[now=@da eny=@uvJ bec=beak] [~] [n=@ud ~]]
:-  %noun
::  As you test each of these, sigpam them to add another.
=/  m0  (zeros:lmd 3 5)
=/  m1  (ones:lmd 5 4)
::=/  mr  (make:lvm `(list @rd)`~[.~1 .~2 .~3 .~4 .~5])
~&  `@ux`m0
~&  (shape:lmd m0)
~&  `@ux`m1
~&  (shape:lmd m1)
::~&  mr
1
::(make:lvd 5)

:: Is there a way to pin zero-bits onto a value?  Holding zeros at the top of an atom is not trivial unless we pin the length on first...

:: To convert, change @rd to @rs etc. and 6 to 5 throughout.  Rename @lvd to @lvs.
